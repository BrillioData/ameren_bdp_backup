CREATE DATABASE IF NOT EXISTS socialmedia;
USE socialmedia;

CREATE EXTERNAL TABLE IF NOT EXISTS tblAge ( userAgeBracket String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblAge';


CREATE EXTERNAL TABLE IF NOT EXISTS tblBehaviourAndNetwork ( userType String, hostname String,networkDomain String,date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblBehaviourAndNetwork';


CREATE EXTERNAL TABLE IF NOT EXISTS tblGender ( userGender String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblGender';


CREATE EXTERNAL TABLE IF NOT EXISTS tblGeo ( latitude String, longitude String,country String,city String,region String,metro String,date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblGeo';


CREATE EXTERNAL TABLE IF NOT EXISTS tblTechnologyBrowser ( browser String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblTechnologyBrowser';



CREATE EXTERNAL TABLE IF NOT EXISTS tblMobile ( deviceCategory String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblMobile';


CREATE EXTERNAL TABLE IF NOT EXISTS tblTechnologyOperatingSystem ( operatingSystem String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblTechnologyOperatingSystem';


CREATE EXTERNAL TABLE IF NOT EXISTS tblLanguage ( language String, date String,sessions String,percentNewSessions String,newUsers String,bounceRate String,pageviewsPerSession String,avgSessionDuration String )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION '/socialmedia/googleanalytics/tblLanguage';

