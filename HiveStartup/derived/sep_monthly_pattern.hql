CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS  sep_monthly_pattern (
bill_account int,
rday string,
daily_total float)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/sep_monthly_pattern/';