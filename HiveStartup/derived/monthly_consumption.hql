CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS MONTHLY_CONSUMPTION(MTR_MTRMAN string,M1 string,M2 string,M3 string,M4 string,M5 string,M6 string,M7 string,M8 string,M9 string,M10 string,M11 string,M12 string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/MONTHLY_CONSUMPTION';