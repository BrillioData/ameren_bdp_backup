CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS cs_rdg_hist_bg( premise string,service_point string,meter_point string,bill_group string,service_type string,space_heat string,weather_group string,irregular_use string,primary string,account string,tariff string,tariff_short_desc string,tariff_group string,original_days string,original_usage string,original_time string,original_wfm string,billed_days string,billed_usage string,billed_adj_reason string,billed_time string,billed_wfm string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/cs_rdg_hist_bg';