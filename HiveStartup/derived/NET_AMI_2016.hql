CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS NET_AMI_2016 (Mtr_Prg_Rdg_Typ_Dcode string,CD_MPT_TYPE int, ACCOUNT_ID string,`Electric` string, RDG_DTTM Timestamp,CALC_READING_VALUE string ,READ_INTERVAL string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '~'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/NET_AMI_2016';