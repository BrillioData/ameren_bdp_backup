CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS For_Transmittal_MO_Headcount_D20_21_V1(
ScenarioCode string,RMCDivision string,RMCFunction string,RMCDepartment string,RMCL6Identifier string,RMCL7Identifier string,ResourceType string,YearNumber string,Jan string,Feb string,Mar string,Apr string,May string,Jun string,Jul string,Aug string,Sep string,Oct string,Nov string,Dec string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/MOIMRdataCSV/For_Transmittal_MO_Headcount_D20_21_V1/';