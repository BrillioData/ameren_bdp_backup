CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS Netmeter_AMR_usage ( account_id string,`Electric` string, Reading_DTTM Timestamp,Calc_Reading_Value string ,`1440` string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '~'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/Netmeter_AMR_usage';