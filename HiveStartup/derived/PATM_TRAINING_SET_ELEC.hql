CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS PATM_TRAINING_SET_ELEC(MTR_MTRMAN string,M1 string,M2 string,M3 string,M4 string,M5 string,M6 string,M7 string,M8 string,M9 string,M10 string,M11 string,M12 string,COOLING_1 string,COOLING_2 string,COOLING_3 string,COOLING_4 string,COOLING_5 string,COOLING_6 string,COOLING_7 string,COOLING_8 string,COOLING_9 string,COOLING_10 string,COOLING_11 string,COOLING_12 string,HEAT_1 string,HEAT_2 string,HEAT_3 string,HEAT_4 string,HEAT_5 string,HEAT_6 string,HEAT_7 string,HEAT_8 string,HEAT_9 string,HEAT_10 string,HEAT_11 string,HEAT_12 string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/PATM_TRAINING_SET_ELEC';
