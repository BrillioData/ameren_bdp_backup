CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS Netmeter_AMI_2016_2017 ( ACCOUNT_ID string,`Electric` string, RDG_DTTM Timestamp,CALC_READING_VALUE string ,READ_INTERVAL string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '~'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/Netmeter_AMI_2016-2017';