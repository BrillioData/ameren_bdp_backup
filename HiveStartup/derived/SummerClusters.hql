CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS SummerClusters (bill_account int ,cluster int, hour int , reading_value float)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/SummerClusters/';