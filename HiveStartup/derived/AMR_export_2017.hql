CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS AMR_export_2017 ( Bill_account string,Reading_type string, Reading_datetime Timestamp,Reading_value string ,`Interval` string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/AMR_export_2017';
