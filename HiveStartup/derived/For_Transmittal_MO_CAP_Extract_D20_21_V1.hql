CREATE DATABASE IF NOT EXISTS derived;
USE derived;
CREATE EXTERNAL TABLE IF NOT EXISTS For_Transmittal_MO_CAP_Extract_D20_21_V1(
ScenarioCode string,LeadRMCSegment string,LeadRMCDivision string,LeadRMCFunction string,LeadRMCDepartment string,RMCL6Identifier string,RMCL7Identifier string,CostCatL3 string,CostCategoryL7 string,ProjectReason string,AssetGroup string,CPOCProgramIndex string,ProjectIndex string,BusinessDivisionIndex string,Activity string,ResourceTypeL5 string,RTNameL2 string,YearNumber string,Jan string,Feb string,Mar string,Apr string,May string,Jun string,Jul string,Aug string,Sep string,Oct string,Nov string,Dec string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/MOIMRdataCSV/For_Transmittal_MO_CAP_Extract_D20_21_V1/';