CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS Netmeter_usage_2017 ( account string,type string, Reading_DTTM Timestamp,Calc_Reading_Value string ,`1440` string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/Netmeter_usage_2017';