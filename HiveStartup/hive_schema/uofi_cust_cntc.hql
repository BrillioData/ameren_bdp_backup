CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS uofi_cust_cntc ( ky_rec  BIGINT,dt_cntc string, cd_cntc_type String, mode string,ky_user_id String)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/uofi_cust_cntc/';