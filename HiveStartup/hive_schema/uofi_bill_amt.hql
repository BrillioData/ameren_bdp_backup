CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS uofi_bill_amt ( ky_rec BIGINT,dt_bill string, at_current String,at_arrears string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/uofi_bill_amt/';