create database IF NOT EXISTS twitter_batch;
create EXTERNAL TABLE IF NOT EXISTS twitter_batch.amerentwitter
(tweet String,id String,
RetweetCount string,
created_at  String,
FavoriteCount string,
place  String,
lang String,
latitude  String,longitude  String,retweeted_status String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '|'
LOCATION "wasb://linuxhdinsightclusterameren@sabdpameren.blob.core.windows.net/socialmedia/twitter_batch/GalShoffet";