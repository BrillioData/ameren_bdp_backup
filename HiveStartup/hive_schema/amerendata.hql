CREATE DATABASE IF NOT EXISTS amerendata;

USE amerendata;

CREATE EXTERNAL TABLE IF NOT EXISTS amerendata ( data_id string,data_type string, data_date Timestamp,data_value string ,data_value2 string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@amerenblob.blob.core.windows.net/amerendata/';