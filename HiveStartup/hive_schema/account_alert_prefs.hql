CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS account_alert_prefs (qy_prcs_cntl_data char(10),prefrc_strt_dt char(8),prefrc_end_dt char(8),prefrc_type_cd char(50),description char(250))
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/account_alert_prefs/';