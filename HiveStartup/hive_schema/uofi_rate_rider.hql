CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS uofi_rate_rider ( ky_rec BIGINT,ky_spt string, cd_svc_type string,dt_eff string ,dt_term string,tar_sch_desc string,agreement string )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/uofi_rate_rider/';