CREATE DATABASE IF NOT EXISTS amerendata;

USE amerendata;

CREATE EXTERNAL TABLE amerendata_partitioned (Bill_account string,Reading_type string, Reading_datetime Timestamp,Reading_value string ,`Interval` string) 
PARTITIONED BY (Reading_month String) 
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
STORED AS ORC Location '/amerenORCpartitioned/';
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='1');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='2');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='3');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='4');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='5');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='6');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='7');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='8');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='9');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='10');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='11');
ALTER TABLE amerendata_partitioned ADD PARTITION(Reading_month='12');