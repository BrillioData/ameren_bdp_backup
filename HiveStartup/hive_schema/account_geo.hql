CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS account_geo(qy_prcs_cntl_data char(10), latitude char(30) ,longitude char(30) )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/account_geo/';