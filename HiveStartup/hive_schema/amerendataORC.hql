CREATE DATABASE IF NOT EXISTS amerendata;

USE amerendata;

CREATE EXTERNAL TABLE if not exists amerendataORC ( data_id string,data_type string, data_date Timestamp,data_value string ,data_value2 string ) 
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS ORC Location '/amerendataORC/';