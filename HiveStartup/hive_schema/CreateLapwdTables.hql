create database lapwd;
use lapwd; 
create table LoadProfileChannel(
RequestToken String,
Identifier String,
DeviceClass String,
ScalingType String,
IntervalLength String,
PulseMultiplier String,
Quantity String,
TimeDataEnd String,
ChannelValue String,
ProfileStatus String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','  
STORED AS ORC 
Location 'wasb://processed-data@lapwd.blob.core.windows.net/ProcessedDataORC/april/';
create table RegisterValue(
RequestToken String,
Identifier String,
DeviceClass String,
ScalingType String,
Quantity String,
Source String,
TimeStamp String,
Value String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','  
STORED AS ORC 
Location 'wasb://processed-data@lapwd.blob.core.windows.net/ProcessedDataORC/may/';
create table Quantity(
RequestToken String,quantity String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','  
STORED AS ORC 
Location 'wasb://processed-data@lapwd.blob.core.windows.net/ProcessedDataORC/june/';
create external table LoadProfileChannel_excel(
RequestToken String,
Identifier String,
DeviceClass String,
ScalingType String,
IntervalLength String,
PulseMultiplier String,
Quantity String,
TimeDataEnd String,
ChannelValue String,
ProfileStatus String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','  
Location 'wasb://lapwd-tableau@sabdpdemo.blob.core.windows.net/reports/loadprofilechannel/'
tblproperties ("skip.header.line.count"="1");
create External table Exception (
ElectronicSerialNumber String,
ReceivedWhen String,
ExceptionCategory String,
Name String,
ID String,
Timestamp String,
SequenceNumber String,
Argument_Name String,
Argument_value String)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','
LOCATION 'wasb://processed-data@lapwd.blob.core.windows.net/ProcessedData/ExceptionDataOutput/';
create external table stock (
SYMBOL string,
SERIES string,
OPEN string,
HIGH string,
LOW string,
CLOSE string,
LAST string,
PREVCLOSE string,
TOTTRDQTY string,
TOTTRDVAL string,
TIMESTAMP string,
TOTALTRADES string,
ISIN string)
ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'  
WITH SERDEPROPERTIES ("input.regex" = "\"(.+)\",\"(.+)\",([0-9.]+),([0-9.]+),([0-9.]+),([0-9.]+),([0-9.]+),([0-9.]+),([0-9]+),([0-9.]+),\"(.+)\",([0-9]+),\"(.+)\"$","output.format.string" = "%1$s %2$s %3$s %4$s %5$s %6$s %7$s %8$s %9$s %10$s %11$s %12$s %13$s")
LOCATION 'wasb://stock-data@sabdpdemo.blob.core.windows.net/'
tblproperties ("skip.header.line.count"="1");
create external table stock_sector (
CompanyName string,
Industry string,
Symbol string,
Series string,
ISINCode string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY ','
LOCATION 'wasb://sector-stock@sabdpdemo.blob.core.windows.net/'
tblproperties ("skip.header.line.count"="1");
