CREATE DATABASE IF NOT EXISTS amerendata;
USE amerendata;
CREATE EXTERNAL TABLE IF NOT EXISTS account_periodic_usage (qy_prcs_cntl_data  char(10),ky_mtr_equip_no char(9),cd_per_use char(1),cd_desc char(250))
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'  
LOCATION 'wasb://amerendata@sabdpameren.blob.core.windows.net/amerencustdata/account_periodic_usage/';
