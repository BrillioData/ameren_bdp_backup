cd /usr/bin/anaconda/bin/
export PATH=/usr/bin/anaconda/bin:$PATH
sudo chmod -R 777 /usr/bin/anaconda
conda update -y matplotlib
conda install -y Theano
yes | pip install scikit-neuralnetwork
yes | pip install vaderSentiment