#!/usr/bin/expect

cd /home/admin123/SFTP/sftpdata
spawn sftp Brillio@ftp.ameren.com
expect "password:"
send "ihCtwAu93uAxxKHq\n"
expect "sftp>"
send "get /Brillio/FRED-GGGDTPBRA188N.csv\n"
expect "sftp>"
send "exit\n"
expect ":~$"
spawn hadoop fs -mkdir -p /ftpdata/FRED-GGGDTPBRA188N/
expect ":~$"
spawn hadoop fs -copyFromLocal -f /home/admin123/SFTP/sftpdata/FRED-GGGDTPBRA188N.csv /ftpdata/FRED-GGGDTPBRA188N/
expect ":~$"
spawn python /home/admin123/scripts/GenerateHiveScript.py /home/admin123/SFTP/sftpdata/FRED-GGGDTPBRA188N.csv FRED-GGGDTPBRA188N FRED-GGGDTPBRA188NTable /ftpdata/FRED-GGGDTPBRA188N /home/admin123/SFTP/sftphivequery/hivequery
expect ":~$"
spawn sudo chmod -R 777 /home/admin123/SFTP/sftphivequery/hivequeryFRED-GGGDTPBRA188NTable.hql
expect ":~$"
spawn hive -f /home/admin123/SFTP/sftphivequery/hivequeryFRED-GGGDTPBRA188NTable.hql
expect eof
catch wait result