sudo apt-get -y install python-dev
sudo apt-get -y install python-virtualenv
sudo virtualenv --system-site-packages tensorflow
sudo source tensorflow/bin/activate 
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.10.0-cp27-none-linux_x86_64.whl
sudo pip install --upgrade $TF_BINARY_URL