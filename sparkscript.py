from pyspark.context import SparkContext,SparkConf
sc = SparkContext()
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
sqlCtx = HiveContext(sc)	
sqlCtx.sql('USE amerendata')
ami_no_dup = sqlCtx.sql('SELECT A.account_id, A.rdg_dttm, A.calc_reading_value FROM (SELECT DISTINCT * FROM net_ami_2016 WHERE mtr_prg_rdg_typ_dcode != "Mtr_Prg_Rdg_Typ_Dcode" AND rdg_dttm BETWEEN "2016-01-01 00:00:00" AND "2016-12-31 23:59:59" ORDER BY account_id, rdg_dttm) A')
sqlCtx.sql('CREATE TABLE IF NOT EXISTS ami_no_dup(account_id string, rdg_dttm timestamp, calc_reading_value float)')
from pyspark.sql import DataFrameWriter
dfw1 = DataFrameWriter(ami_no_dup)
dfw1.insertInto('ami_no_dup', overwrite=True)
netmeter_no_dup = sqlCtx.sql('SELECT A.account_id, A.rdg_dttm, A.calc_reading_value FROM (SELECT DISTINCT * FROM netmeter_ami_2016_2017 WHERE rdg_dttm BETWEEN "2016-01-01 00:00:00" AND "2016-12-31 23:59:59"ORDER BY account_id, rdg_dttm) A')
sqlCtx.sql('CREATE TABLE IF NOT EXISTS netmeter_no_dup(account_id string, rdg_dttm timestamp, calc_reading_value float)')
dfw2 = DataFrameWriter(netmeter_no_dup)
dfw2.insertInto('netmeter_no_dup', overwrite=True)
